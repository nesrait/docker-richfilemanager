FROM nimmis/apache-php5

ENV VERSION=2.6.1
RUN wget -qO- https://github.com/servocoder/RichFilemanager/archive/v${VERSION}.tar.gz | tar xvz -C /tmp \
 && mv /tmp/RichFilemanager-${VERSION}/* /var/www/html \
 && chown -R www-data:www-data /var/www/html

RUN cd /var/www/html/config \
 && mv filemanager.init.js.example filemanager.init.js \
 && cp filemanager.config.default.json filemanager.config.json

WORKDIR /var/www/html

USER www-data
RUN composer install

VOLUME /var/www/html/userfiles

USER root
