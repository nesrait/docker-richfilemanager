
Dockerized version of [RichFilemanager](https://github.com/servocoder/RichFilemanager/).


To build:
```
docker build -t richfilemanager .
```


To run:
```
docker run -d -p 8080:80 -v `pwd`:/var/www/html/userfiles richfilemanager
```
